from flask import Flask
app = Flask(__name__)

@app.route('/')

def index():
    return 'Hello to my deployed site'

if __name__ == '__main__':
    app.run(port=5000)